'use strict';

var container = document.querySelector('#container');
var modalWindow = document.querySelector('#modalWindow');
var closeWindowButton = document.querySelector('#closeWindowButton');
var modalOpenButton = document.querySelector('#modalOpenButton');


container.addEventListener('click', function(event){
    event.preventDefault();
    if(event.target.id == 'modalWindow' || event.target.id == 'modalOpenButton') {
        modalWindow.style.display ='block';
        closeWindowButton.style.display = 'block';
        modalOpenButton.style.display = 'none';
    } else {
        modalWindow.style.display ='none';
        closeWindowButton.style.display = 'none';
        modalOpenButton.style.display = 'block';
    }
});