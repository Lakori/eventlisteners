let accordContainer = document.querySelector('#accord-container');

let title = accordContainer.querySelectorAll('.accordion-title');

for(let i =0; i < title.length; i++){
   
    title[i].addEventListener('click', function(event){
        event.preventDefault();
        if(!(this.classList.contains('active'))){
            for(let i = 0; i < title.length; i++){
                title[i].classList.remove('active');
            }
                this.classList.add('active'); 

        } else{
            this.classList.remove('active');
        }
        })
}
